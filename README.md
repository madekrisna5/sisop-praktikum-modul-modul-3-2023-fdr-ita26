# Sisop Modul 3 <br> Kelompok ITA 26



## Soal 1

### Cara Kerja
Kode tersebut merupakan program untuk melakukan kompresi data dengan algoritma lossless compression. Program ini mengimplementasikan algoritma Huffman Coding untuk menghasilkan kode biner yang merepresentasikan setiap karakter dalam data yang diinputkan.

Program ini terdiri dari beberapa fungsi:

1. calculate_frequency() : untuk menghitung frekuensi kemunculan setiap karakter dalam data yang diinputkan.
2. encode() : untuk menghitung probabilitas dan kode Huffman untuk setiap karakter berdasarkan frekuensi kemunculan yang dihitung pada calculate_frequency(), dan menulis kode Huffman ke dalam pipa untuk diproses pada proses berikutnya.
3. compress() : untuk membaca data yang akan dikompresi dari file input, membaca kode Huffman dari pipa, dan menulis data yang telah dikompresi ke dalam file output.
4. decompress() : untuk membaca kode Huffman dari pipa, dan menuliskan data yang telah didekompresi ke dalam file output.
5. main() : untuk memanggil fungsi-fungsi tersebut dan mengatur jalannya program.

Program ini menggunakan teknik fork untuk membuat dua proses terpisah: satu proses untuk menghitung kode Huffman dan satu proses untuk melakukan kompresi dan dekompresi. Selain itu, program juga menggunakan dua pipa (pipe) untuk mengirim kode Huffman dari proses pertama ke proses kedua dan untuk mengirim data yang telah dikompresi dari proses kedua ke proses pertama.

### Screenshot File
![Foto](/Foto/S1_F1.png )

### Screenshot Kode
![Foto](/Foto/S1_K1.png )
![Foto](/Foto/S1_K2.png )
![Foto](/Foto/S1_K3.png )
![Foto](/Foto/S1_K4.png )


### Screenshot Program

- Decompressed <br>
![Foto](/Foto/S1_P1.png )
- compressed <br>
![Foto](/Foto/S1_P1.png )

### Kendala Pengerjaan
- Frekuensi huruf soal masih belum ditunjukan pada output
- Belum visualisasi algoritma huffman
- Belum ada perbandingan bit sebelum dan sesudah konversi

## Soal 2

### Cara Kerja
- Cinta <br>
Kode ini adalah program C yang menggunakan shared memory (memori bersama) untuk berbagi data antara proses yang berbeda. Program ini menciptakan sebuah matriks dengan ukuran ROW1 x COL2, kemudian menyimpannya di shared memory. Setelah itu, program menampilkan isi matriks dengan memanggil fungsi printMatrix(). Kemudian program melepaskan memori bersama dan menghapusnya dari sistem.

Berikut ini adalah penjelasan singkat dari setiap bagian kode:

1. #include <stdio.h> dan <stdlib.h>: Mendefinisikan fungsi standar untuk input/output dan alokasi memori
2. <sys/shm.h>: Header file untuk shared memory functions
3. #define ROW1 4 dan #define COL2 5: Mendefinisikan ukuran matriks sebagai konstanta
4. void printMatrix(int mat[][COL2]): Fungsi untuk menampilkan matriks ke layar
5. key_t key = ftok("kalian.c", 1234);: Menciptakan sebuah key untuk shared memory. ftok() digunakan untuk menghasilkan key dari sebuah file dan sebuah integer.
6. int shmid = shmget(key, sizeof(int[ROW1][COL2]), 0666 | IPC_CREAT);: Mengalokasikan memori bersama untuk menyimpan matriks dengan ukuran ROW1 x COL2, kemudian mengembalikan ID memori bersama yang baru saja dibuat.
7. int (*mat_res)[COL2] = (int(*)[COL2])shmat(shmid, NULL, 0);: Mengambil alamat memori bersama dan mengkonversinya menjadi tipe data matriks.
8. printf("Result:\n"); printMatrix(mat_res);: Menampilkan isi matriks ke layar dengan memanggil fungsi printMatrix().
9. shmdt(mat_res); shmctl(shmid, IPC_RMID, NULL);: Melepaskan memori bersama dan menghapusnya dari sistem.



- Kalian <br>
Kode tersebut merupakan program untuk mengalikan dua buah matriks yang memiliki ukuran masing-masing sebanyak ROW1 x COL1 dan ROW2 x COL2, dan menyimpan hasil perkalian dalam sebuah matriks hasil dengan ukuran ROW1 x COL2.

Pada fungsi multiplyMatrix(), dilakukan perulangan untuk setiap baris dan kolom pada matriks hasil yang ingin diisi dengan hasil perkalian. Setiap elemen pada matriks hasil diinisialisasi dengan nilai 0 dan kemudian dihitung hasil perkaliannya dengan mengalikan setiap elemen pada baris i pada matriks pertama dengan setiap elemen pada kolom j pada matriks kedua. Proses perkalian dilakukan dengan perulangan pada k dari 0 hingga COL1.

Selanjutnya pada fungsi printMatrix(), dilakukan perulangan untuk setiap baris dan kolom pada matriks yang ingin dicetak, dan setiap elemennya dicetak ke layar menggunakan printf().

Pada fungsi main(), terdapat pembuatan tiga buah matriks yaitu mat1, mat2, dan res. Selain itu, program menggunakan fungsi srand(time(0)) untuk menghasilkan nilai acak berdasarkan waktu saat ini untuk diisi pada matriks mat1 dan mat2. Setelah itu, dilakukan pemanggilan fungsi multiplyMatrix() untuk mengalikan dua matriks dan hasilnya disimpan pada matriks res. Kemudian hasil dari masing-masing matriks dicetak ke layar menggunakan fungsi printMatrix(). Akhirnya, program mengembalikan nilai 0 sebagai tanda bahwa program berjalan dengan sukses.

- Sisop <br>
Kode ini merupakan program C yang menggunakan shared memory untuk mengirimkan matriks hasil perkalian dua matriks dari program lain. Program ini menerima matriks hasil perkalian dari shared memory dan mencetak matriks tersebut. Selain itu, program ini juga menghitung nilai faktorial dari setiap elemen matriks dan mencetak hasilnya.

Secara lebih rinci, program ini menggunakan fungsi ftok() untuk mendapatkan key unik yang digunakan untuk mengidentifikasi shared memory segment yang sama dengan program lain. Kemudian, program ini menggunakan fungsi shmget() untuk membuat shared memory segment dengan ukuran yang sesuai dan mendapatkan identifikasinya dalam bentuk integer (shmid). Selanjutnya, program ini menggunakan fungsi shmat() untuk menautkan shared memory segment ke dalam memori program dan mengembalikan alamatnya sebagai pointer ke matriks mat_res.

Setelah menerima matriks dari shared memory, program ini mencetak matriks tersebut menggunakan fungsi printMatrix(). Kemudian, program ini menghitung nilai faktorial dari setiap elemen matriks menggunakan fungsi factorial(). Hasil faktorial kemudian dicetak menggunakan printf(). Setelah selesai digunakan, program ini menggunakan fungsi shmdt() untuk melepaskan shared memory dari memori program dan shmctl() untuk menghapus shared memory segment dari sistem.

### Screenshot File
![Foto](/Foto/S2_F1.png )

### Screenshot Kode
- Cinta <br>
![Foto](/Foto/S2_K11.png )<br>

- Kalian <br>
![Foto](/Foto/S2_K21.png )<br>
![Foto](/Foto/S2_K22.png )<br>

- Sisop <br>
![Foto](/Foto/S2_K31.png )<br>

### Screenshot Program
- Cinta <br>
![Foto](/Foto/S2_P1.png )
- Kalian <br>
![Foto](/Foto/S2_P2.png )
- Sisop <br>
![Foto](/Foto/S2_P3.png )

### Kendala Pengerjaan
- Belum menampilkan a dan b matriks
- File Sisop.c belum menunjukan directory baru


## Soal 3

### Cara Kerja
- Stream <br>
Kode ini merupakan implementasi sebuah program dalam bahasa C yang mengelola sebuah playlist lagu dalam sebuah file bernama playlist.txt. Program ini menggunakan beberapa library seperti stdio.h, stdlib.h, string.h, unistd.h, fcntl.h, sys/types.h, sys/ipc.h, sys/msg.h, dan sys/sem.h.

Pada awal kode, terdapat definisi beberapa konstanta seperti MAX_SONGS dan MAX_LENGTH yang digunakan untuk menentukan jumlah maksimum lagu pada playlist dan panjang maksimum dari nama lagu. Kemudian, terdapat dua struktur data yaitu message yang digunakan untuk menyimpan pesan dari user dan song yang digunakan untuk menyimpan informasi lagu.

Setelah definisi konstanta dan struktur data, terdapat deklarasi beberapa fungsi yang akan digunakan di program utama seperti decryptPlaylist(), listSongs(), playSong(char *songName), addSong(char *songName), dan isSongOnPlaylist(char *songName). Kemudian, terdapat fungsi getNumSongs() yang mengembalikan jumlah lagu pada playlist.

Fungsi main() adalah fungsi utama pada program ini. Pertama-tama, program membuat sebuah key untuk message queue menggunakan fungsi ftok(). Kemudian, program membuat message queue menggunakan fungsi msgget(), membuat key untuk semaphore, dan membuat semaphore menggunakan fungsi semget() dan semctl().

Selanjutnya, program akan menerima pesan dari message queue menggunakan fungsi msgrcv() dalam sebuah loop. Pesan tersebut kemudian akan diproses menggunakan decryptPlaylist(), listSongs(), playSong(char *songName), atau addSong(char *songName) tergantung dari perintah yang diterima. Jika perintah tidak dikenali, program akan mencetak UNKNOWN COMMAND. Program akan mengecek apakah sistem overload dengan memeriksa jumlah lagu pada playlist menggunakan getNumSongs(). Jika jumlah lagu pada playlist melebihi MAX_SONGS dan semaphore bernilai 0, program akan mencetak STREAM SYSTEM OVERLOAD dan keluar dari loop.

Setelah selesai diproses, message queue dan semaphore akan dihapus menggunakan fungsi msgctl() dan semctl().

- User <br>
Kode tersebut merupakan program dalam bahasa C yang menggunakan message queue untuk mengirim pesan ke sistem stream. Program ini terdiri dari beberapa perintah yang dikirim ke sistem stream dalam bentuk pesan melalui message queue. Adapun langkah-langkah yang dilakukan oleh program tersebut secara singkat adalah sebagai berikut:

1. Membuat key dengan menggunakan fungsi ftok() untuk menghasilkan kunci unik berdasarkan path dan karakter 's'.
2. Mengambil id message queue dengan menggunakan fungsi msgget() untuk menghasilkan ID message queue baru atau menggunakan ID message queue yang sudah ada.
3. Mengirim perintah DECRYPT, LIST, PLAY, ADD, dan UNKNOWN COMMAND ke sistem stream dalam bentuk pesan menggunakan fungsi msgsnd().
4. Menutup koneksi dengan sistem stream dengan mengirim perintah EXIT ke dalam message queue.
5. Keluar dari program.

Perintah-perintah yang dikirim ke sistem stream mungkin digunakan untuk mengendalikan pemutaran lagu, menambahkan atau menghapus lagu dari playlist, atau melakukan operasi lainnya yang terkait dengan sistem streaming musik.


### Screenshot File
![Foto](/Foto/S3_F1.png )

### Screenshot Kode
- Stream <br>
![Foto](/Foto/S3_K11.png ) <br>
![Foto](/Foto/S3_K12.png ) <br>
![Foto](/Foto/S3_K13.png ) <br>
![Foto](/Foto/S3_K14.png ) <br>
![Foto](/Foto/S3_K15.png ) <br>
![Foto](/Foto/S3_K16.png ) <br>
![Foto](/Foto/S3_K17.png ) <br>
- User <br>
![Foto](/Foto/S3_K21.png ) <br>
![Foto](/Foto/S3_K22.png ) <br>

### Screenshot Program
- Stream dan User
![Foto](/Foto/S3_P1.png )



### Kendala Pengerjaan
- File tidak bisa connect pada server


## Soal 4

### Cara Kerja

- Categorize <br>
Kode ini adalah sebuah program C yang dapat digunakan untuk mengkategorikan berkas dalam suatu folder ke dalam folder yang berbeda berdasarkan ekstensi dari berkas tersebut. Program ini menerima input dari pengguna berupa dua folder, yaitu folder sumber (source_folder) dan folder tujuan (dest_folder). Program akan memindahkan semua berkas yang berada di dalam folder sumber ke dalam folder tujuan dan memisahkan berkas-berkas tersebut ke dalam subfolder-subfolder yang dibuat berdasarkan ekstensi berkas-berkas tersebut.

Program ini menggunakan beberapa fungsi, antara lain:

1. log_access(): untuk mencatat setiap kali folder diakses.
2. log_move(): untuk mencatat setiap kali berkas dipindahkan.
3. log_make(): untuk mencatat setiap kali folder dibuat.
4. is_directory(): untuk mengecek apakah sebuah path adalah sebuah direktori.
5. create_folder(): untuk membuat sebuah folder.
6. categorize_file(): untuk mengkategorikan berkas-berkas dalam folder sumber dan memindahkannya ke folder tujuan sesuai dengan ekstensinya.



- Logchecker <br>
Program ini merupakan program C yang membaca sebuah file log dengan nama "log.txt" dan melakukan beberapa operasi untuk menghasilkan beberapa informasi yang relevan dari log tersebut.

Program ini terdiri dari beberapa fungsi yaitu count_accessed_folders, count_files_per_extension, dan compare_folder_info yang masing-masing memiliki tugasnya masing-masing.

Fungsi count_accessed_folders membaca setiap baris dari file log dan menghitung jumlah folder yang diakses dengan mencari baris yang mengandung kata "ACCESSED". Fungsi ini mengambil argumen pointer ke sebuah variabel accessed_folders dan akan menambahkan 1 setiap kali ditemukan baris yang sesuai.

Fungsi count_files_per_extension juga membaca setiap baris dari file log dan menghitung jumlah file dengan ekstensi tertentu yang dipindahkan ke setiap folder dengan ekstensi yang sama. Fungsi ini mengambil argumen pointer ke array folders, pointer ke variabel total_files, dan integer folder_count. folders berisi informasi tentang setiap folder dan jumlah file yang dipindahkan ke folder tersebut, total_files berisi jumlah total file yang dipindahkan, dan folder_count berisi jumlah folder yang dipantau. Fungsi ini akan memeriksa baris yang mengandung kata "MOVED", memeriksa ekstensi file yang dipindahkan, dan menambahkan 1 ke file_count pada FolderInfo yang sesuai di dalam array folders.

Fungsi compare_folder_info adalah fungsi pembanding yang digunakan untuk melakukan pengurutan array folders dengan menggunakan fungsi bawaan C yaitu qsort. Fungsi ini membandingkan dua elemen dari tipe data FolderInfo berdasarkan nama folder masing-masing.

Fungsi main adalah fungsi utama program. Pertama-tama, program membuka file log.txt untuk dibaca. Jika gagal membuka file, program akan mengeluarkan pesan kesalahan dan keluar dengan status 1.

Program kemudian menghitung jumlah folder yang diakses dengan memanggil fungsi count_accessed_folders, dan mencetak jumlah folder tersebut.

Program kemudian menghitung jumlah file per ekstensi dan mengisi array folders dengan memanggil fungsi count_files_per_extension. Setelah itu, program melakukan pengurutan array folders dengan memanggil fungsi qsort dengan menggunakan compare_folder_info sebagai pembanding. Program mencetak daftar folder dan jumlah file yang dipindahkan ke setiap folder, serta total file per ekstensi.

Setelah selesai, program menutup file log dan melepaskan memori yang dialokasikan untuk array folders. Program mengembalikan status 0 untuk menandakan bahwa program telah berhasil dieksekusi dengan sukses.

- Unzip <br>
Kode tersebut adalah program C yang menggunakan sistem pemrosesan untuk menjalankan perintah unzip pada file bernama "hehe.zip".

Pada baris 6, program membuat proses baru dengan memanggil fungsi fork(). Pada proses anak, perintah execvp() digunakan untuk menjalankan perintah unzip dengan argumen yang didefinisikan dalam array unzip_args. Jika execvp() gagal, maka proses anak akan keluar dengan status EXIT_FAILURE.

Pada proses induk, program akan menunggu sampai proses anak selesai dengan memanggil waitpid(). Jika proses anak selesai dengan status keluaran 0, maka program akan mencetak pesan "File has been successfully unzipped.". Jika tidak, maka program akan mencetak pesan "Failed to unzip the file.". Jika fork() gagal, maka program akan mencetak pesan "Failed to create child process.".

### Screenshot File
![Foto](/Foto/S4_F1.png )

### Screenshot Kode
- Categorize <br>
![Foto](/Foto/S4_K11.png ) <br>
![Foto](/Foto/S4_K12.png ) <br>
![Foto](/Foto/S4_K13.png ) <br>
![Foto](/Foto/S4_K14.png ) <br>
![Foto](/Foto/S4_K15.png ) <br>
![Foto](/Foto/S4_K16.png ) <br>
- Logchecker <br>
![Foto](/Foto/S4_K21.png ) <br>
![Foto](/Foto/S4_K22.png ) <br>
![Foto](/Foto/S4_K23.png ) <br>
- Unzip <br>
![Foto](/Foto/S4_K31.png ) <br>

### Screenshot Program
- Categorize <br>
![Foto](/Foto/S4_P1.png )
- Logchecker <br>
![Foto](/Foto/S4_P2.png )
- Unzip <br>
![Foto](/Foto/S4_P3.png )

### Kendala Pengerjaan
- Tidak bisa unzip file dari server
